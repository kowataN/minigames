﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace State {
    public class StateLoop : StateBase {
        private SubState _subState = default;

        public StateLoop(executeState execute) : base(execute) {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute() {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        private void ExecuteInit() {
            GameManager.Instance.LockManager.UnLock();

            _subState.ChangeState(ExecuteTurnStartAnime);
        }

        private void ExecuteTurnStartAnime() {
            GameManager.Instance.LockManager.Lock();
            BattleManager.Instance.PlayStartTurnEffect();

            _subState.ChangeState(ExecuteEndTurnStartAnime);
        }

        private void ExecuteEndTurnStartAnime() {
            if (BattleManager.Instance.IsEndEffect()) {
                GameManager.Instance.LockManager.UnLock();
                _subState.ChangeState(ExecuteLoop);
            }
        }

        private void ExecuteLoop() {
            if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                // 敵AI起動
                EnemyPresenter? ep = BattleManager.Instance.EnemyPresenter as EnemyPresenter;
                ep?.Think();
            }

            if (BattleManager.Instance.IsBothDecided()) {
                _subState.ChangeState(ExecuteOpenCardEffect);
            }
        }

        private void ExecuteOpenCardEffect() {
            GameManager.Instance.LockManager.Lock();
            BattleManager.Instance.PlayOpenCardEffect();

            _subState.ChangeState(ExecuteOpenCard);
        }

        private void ExecuteOpenCard() {
            if (BattleManager.Instance.IsEndEffect()) {
                GameManager.Instance.LockManager.UnLock();

                if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                    EnemyPresenter? ep = BattleManager.Instance.EnemyPresenter as EnemyPresenter;
                    ep?.TurnOverCard(true, ExecuteJudge);
                } else {
                    MultiPlayerPresenter? mp = BattleManager.Instance.EnemyPresenter as MultiPlayerPresenter;
                    mp?.TurnOverCard(true, ExecuteJudge);
                }

                GameManager.Instance.LockManager.Lock();

                _subState.ChangeState(null);
            }
        }

        private void ExecuteJudge() {
            GameManager.Instance.LockManager.UnLock();

            var ret = BattleManager.Instance.PlayerPresenter.Attack(BattleManager.Instance.EnemyPresenter);
            BattleManager.Instance.PlayerPresenter.SetAttackResult(ret);
            DebugView.Instance?.SetPlayerAttackLog(
                ret == Entity.AttackResult.Success ? "攻撃成功"
                : ret == Entity.AttackResult.Failure ? "攻撃失敗"
                : "あいこ");

            ret = BattleManager.Instance.EnemyPresenter.Attack(BattleManager.Instance.PlayerPresenter);
            BattleManager.Instance.EnemyPresenter.SetAttackResult(ret);
            DebugView.Instance?.SetEnemyAttackLog(
                ret == Entity.AttackResult.Success ? "攻撃成功"
                : ret == Entity.AttackResult.Failure ? "攻撃失敗"
                : "あいこ");

            GameManager.Instance.LockManager.Lock();

            _subState.ChangeState(ExecuteEndJudge);
        }

        private void ExecuteEndJudge() {
            // 変動値
            var floatingValue = BattleManager.Instance.BattleData.FloatingPoint;

            _subState.ChangeState(null);

            // プレイヤー攻撃
            var atkRet = BattleManager.Instance.PlayerPresenter.GetAttackResult();
            if (atkRet == Entity.AttackResult.Failure) {
                floatingValue *= -1;
            }
            else if (atkRet == Entity.AttackResult.Draw) {
                floatingValue = 0;
            }

            BattleManager.Instance.PlayerPresenter
                .AddPointPresenter.PlayAddPoint(Entity.AttackType.Attack, floatingValue, ExecuteEndAttackEffect);
            BattleManager.Instance.EnemyPresenter
                .AddPointPresenter.PlayAddPoint(Entity.AttackType.Defense, -floatingValue, () => { });
        }

        private void ExecuteEndAttackEffect() {
            GameManager.Instance.LockManager.UnLock();

            //変動値
            var floatingValue = BattleManager.Instance.BattleData.FloatingPoint;

            // ポイント変動
            var atkRet = BattleManager.Instance.PlayerPresenter.GetAttackResult();
            if (atkRet == Entity.AttackResult.Failure) {
                floatingValue *= -1;
            } else if (atkRet == Entity.AttackResult.Draw) {
                floatingValue = 0;
            }

            BattleManager.Instance.PlayerPresenter.AddPoint(floatingValue);
            BattleManager.Instance.EnemyPresenter.AddPoint(-floatingValue);

            GameManager.Instance.LockManager.Lock();
            _subState.ChangeState(ExecuteEndFloatingAttackPoint);
        }

        private void ExecuteEndFloatingAttackPoint() {
            // 変動値
            var floatingValue = BattleManager.Instance.BattleData.FloatingPoint;

            _subState.ChangeState(null);

            // エネミー攻撃
            var atkRet = BattleManager.Instance.EnemyPresenter.GetAttackResult();
            if (atkRet == Entity.AttackResult.Failure) {
                floatingValue *= -1;
            } else if (atkRet == Entity.AttackResult.Draw) {
                floatingValue = 0;
            }

            BattleManager.Instance.EnemyPresenter
                .AddPointPresenter.PlayAddPoint(Entity.AttackType.Attack, floatingValue, ExecuteEndDefenseEffect);
            BattleManager.Instance.PlayerPresenter
                .AddPointPresenter.PlayAddPoint(Entity.AttackType.Defense, -floatingValue, () => { });
        }

        private void ExecuteEndDefenseEffect() {
            //変動値
            var floatingValue = BattleManager.Instance.BattleData.FloatingPoint;

            var atkRet = BattleManager.Instance.EnemyPresenter.GetAttackResult();
            if (atkRet == Entity.AttackResult.Failure) {
                floatingValue *= -1;
            } else if (atkRet == Entity.AttackResult.Draw) {
                floatingValue = 0;
            }

            BattleManager.Instance.PlayerPresenter.AddPoint(-floatingValue);
            BattleManager.Instance.EnemyPresenter.AddPoint(floatingValue);

            GameManager.Instance.LockManager.Lock();
            _subState.ChangeState(ExecuteBattleEndJudge);
        }

        private void ExecuteBattleEndJudge() {
            GameManager.Instance.LockManager.UnLock();

            if (BattleManager.Instance.IsLastTurn()) {
                _subState.ChangeState(ExecuteEndBattle);
            } else {
                // ターン進行
                BattleManager.Instance.AddCurrentTurn();

                GameManager.Instance.LockManager.Lock();

                _subState.ChangeState(ExecuteCloseCard);
            }
        }

        private void ExecuteCloseCard() {
            if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                EnemyPresenter? ep = BattleManager.Instance.EnemyPresenter as EnemyPresenter;
                ep?.TurnOverCard(false, ExecuteInitHandCard);
            } else {
                MultiPlayerPresenter? mp = BattleManager.Instance.EnemyPresenter as MultiPlayerPresenter;
                mp?.TurnOverCard(false, ExecuteInitHandCard);
            }

            _subState.ChangeState(null);
        }

        private void ExecuteInitHandCard() {
            SoundManager.Instance.PlayMoveCard();
            BattleManager.Instance.InitHandCardPosition();
            BattleManager.Instance.InitDecideButton();
            if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                EnemyPresenter? ep = BattleManager.Instance.EnemyPresenter as EnemyPresenter;
                ep?.InitThink();
            }

            _subState.ChangeState(ExecuteInit);
        }

        private void ExecuteEndBattle() {
            _subState.ChangeState(null);

            StateEnd = true;
        }
    }
}
