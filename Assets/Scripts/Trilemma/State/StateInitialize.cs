﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateInitialize : StateBase
    {
        private SubState _subState = default;

        public StateInitialize(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        private void ExecuteInit()
        {
//            Fader.Instance.SetColorAndActive(Color.black, true);

            // 初期化処理
            BattleManager.Instance.Initialize();

            StateEnd = true;
        }
    }
}
