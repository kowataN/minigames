﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateDecideFirstAttack : StateBase
    {
        private SubState _subState = default;

        public StateDecideFirstAttack(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        /// <summary>
        /// 各種初期化
        /// </summary>
        private void ExecuteInit()
        {
            _subState.ChangeState(ExecuteIdle);
        }

        /// <summary>
        /// カード選択待機
        /// </summary>
        private void ExecuteIdle()
        {
            _subState.ChangeState(ExecuteOpenCard);
        }

        /// <summary>
        /// 相手のカードオープン
        /// </summary>
        private void ExecuteOpenCard()
        {
            _subState.ChangeState(ExecuteStartResultAnime);
        }

        /// <summary>
        /// 結果アニメ再生
        /// </summary>
        private void ExecuteStartResultAnime()
        {
            _subState.ChangeState(ExecuteStateEnd);
        }

        /// <summary>
        /// ステート終了
        /// </summary>
        private void ExecuteStateEnd()
        {
            StateEnd = true;
        }
    }
}
