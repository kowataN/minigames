﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateBattleStartEffect : StateBase
    {
        private SubState _subState = default;

        public StateBattleStartEffect(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        private void ExecuteInit()
        {
            GameManager.Instance.LockManager.Lock();
            BattleManager.Instance.PlayGameStartEffect();

            _subState.ChangeState(ExecuteIdle);
        }

        private void ExecuteIdle()
        {
            if (BattleManager.Instance.IsEndEffect())
            {
                GameManager.Instance.LockManager.UnLock();

                _subState.ChangeState(ExecuteEnd);
            }
        }

        private void ExecuteEnd()
        {
            StateEnd = true;

            _subState.ChangeState(null);
        }
    }
}
