﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateMenuIdle : StateBase
    {
        private SubState _subState = default;

        public StateMenuIdle(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        private void ExecuteInit()
        {
            BattleManager.Instance.ShowMenu();

            _subState.ChangeState(ExecuteIdle);
        }

        private void ExecuteIdle()
        {
            if (BattleManager.Instance.GotoTitle)
            {
                _subState.ChangeState(null);
                StateEnd = true;
            }
        }
    }
}

