﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State {
    public class StateBattleResult : StateBase {
        private SubState _subState = default;

        public StateBattleResult(executeState execute) : base(execute) {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute() {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            if (ExecDelegate != null) {
                ExecDelegate();
            }
        }

        private void ExecuteInit() {
            GameManager.Instance.LockManager.Lock();
            BattleManager.Instance.PlayBattleResultEffect();

            _subState.ChangeState(ExecuteIdle);
        }

        private void ExecuteIdle() {
            if (BattleManager.Instance.IsEndEffect()) {
                GameManager.Instance.LockManager.UnLock();

                _subState.ChangeState(ExecuteEnd);
            }
        }

        private void ExecuteEnd() {
            StateEnd = true;

            _subState.ChangeState(null);
        }
    }
}
