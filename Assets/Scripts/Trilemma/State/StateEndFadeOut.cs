﻿namespace State
{
    public class StateEndFadeOut : StateBase
    {
        private SubState _subState = default;

        public StateEndFadeOut(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        private void ExecuteInit()
        {
            StateEnd = false;
            Fader.Instance.BlackOut(1.5f, () => {
                StateEnd = true;
            });
            _subState.ChangeState(null);
        }
    }
}
