﻿using State;

public class StateManager : SingletonMonoBhv<StateManager> {
    private StateBase _state = default;

    public void Initialize() {
        _state = new StateInitialize(StateInit);
    }

    /// <summary>
    /// ステートマシンを実行します。
    /// </summary>
    public void Execute() {
        if (_state != null) {
            DebugView.Instance?.SetMainStateString(_state.GetStateName());
            _state.Execute();
        }
    }

    /// <summary>
    /// 起動初期化を行います。
    /// </summary>
    private void StateInit() {
        if (_state.StateEnd) {
            _state = new StateStartFadeIn(StateStartFadeIn);
        }
    }

    /// <summary>
    /// 開始フェードインを行います。
    /// </summary>
    private void StateStartFadeIn() {
        if (_state.StateEnd) {
            _state = new StateDecideFirstAttack(StateDecideFirstAttack);
        }
    }

    /// <summary>
    /// 先攻決め
    /// </summary>
    private void StateDecideFirstAttack() {
        if (_state.StateEnd) {
            _state = new StateBattleStartEffect(StateStartEffect);
        }
    }

    /// <summary>
    /// バトル開始演出を行います。
    /// </summary>
    private void StateStartEffect() {
        if (_state.StateEnd) {
            _state = new StateLoop(StateBattleLoop);
        }
    }

    /// <summary>
    /// バトルループを行います。
    /// </summary>
    private void StateBattleLoop() {
        if (_state.StateEnd) {
            _state = new StateBattleEndEffect(StateEndEffect);
        }
    }

    /// <summary>
    /// バトル終了演出を行います。
    /// </summary>
    private void StateEndEffect() {
        if (_state.StateEnd) {
            _state = new StateBattleResult(StateBattleResult);
        }
    }

    private void StateBattleResult() {
        if (_state.StateEnd) {
            _state = new StateMenuIdle(StateMenuIdle);
        }
    }

    private void StateMenuIdle() {
        if (_state.StateEnd) {
            _state = new StateEndFadeOut(StateEndFadeOut);
        }
    }

    private void StateEndFadeOut() {
        if (_state.StateEnd) {
            _state.ChangeState(null);
            BattleManager.Instance.BattleEnd();
            BattleManager.Instance.InitDecideButton();

            GameManager.Instance.SetViewPosition(Entity.ViewType.Title);
        }
    }
}
