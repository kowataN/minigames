﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateStartFadeIn : StateBase
    {
        private SubState _subState = default;

        public StateStartFadeIn(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetSubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        private void ExecuteInit()
        {
            Fader.Instance.BlackIn(2.0f, () => {
                Fader.Instance.SetActiveFade(false);
                StateEnd = true;
            });
            _subState.ChangeState(null);
        }
    }
}
