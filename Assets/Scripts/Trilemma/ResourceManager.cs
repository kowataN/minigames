﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceManager : SingletonMonoBhv<ResourceManager>
{
    [SerializeField] private Sprite[] _cardIconList;
    public Sprite[] CardIconList => _cardIconList;

    [SerializeField] private Sprite[] _stampIconList;
    public Sprite[] StempIconList => _stampIconList;
}
