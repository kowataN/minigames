﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TitleView : MonoBehaviour {
    [SerializeField] private Button _btnTapStart = default;
    [SerializeField] private Button _btnSetting = default;
    [SerializeField] private Text _txtTapStart = default;
    [SerializeField] private GameObject _settingDialog = default;

    [Header("ゲームモード")]
    [SerializeField] private GameObject _gameMode = default;
    [SerializeField] private Button _btnCpu = default;
    [SerializeField] private Button _btnOnline = default;

    private SettingDialogView? _settingDialogView = default;

    private enum TitleMode {
        Start, GameMode,
    };
    private TitleMode _titleMode = TitleMode.Start;

    private void Awake() {
        Init();
    }

    public void Init() {
        SaveDataManager.Instance.LoadAudio();

        _settingDialogView = _settingDialog.GetComponent<SettingDialogView>();
        _settingDialogView.OnClickBack += new Action(() => {
            // サウンド更新
            SaveDataManager.Instance.SaveAudio();

            _settingDialog.SetActive(false);
        });

        SetTitleMode(TitleMode.Start);
        BattleManager.Instance.LoadBattleData();
    }

    private void SetTitleMode(TitleMode mode) {
        switch (mode) {
            case TitleMode.Start:
                _gameMode.SetActive(false);
                _txtTapStart.gameObject.SetActive(true);
                _btnTapStart.interactable = true;
                _settingDialog.SetActive(false);
                break;

            case TitleMode.GameMode:
                _settingDialog.SetActive(false);
                _gameMode.SetActive(true);
                _txtTapStart.gameObject.SetActive(false);
                _btnTapStart.interactable = false;
                break;
        }
        _titleMode = mode;
    }

    public void OnClickCpu() {
        GameManager.Instance.BattleMode = Entity.BattleMode.Solo;
        GameManager.Instance.SetViewPosition(Entity.ViewType.Setting);
    }

    public void OnClickOnline() {
        GameManager.Instance.BattleMode = Entity.BattleMode.Multi;
        GameManager.Instance.SetViewPosition(Entity.ViewType.Matching);
    }

    public void OnClickTapStart() {
        if (GameManager.Instance.LockManager.IsLock()) {
            return;
        }

        SetTitleMode(TitleMode.GameMode);
    }

    public void OnClickSetting() {
        if (_settingDialog.activeSelf) {
            return;
        }

        _settingDialog.SetActive(true);
    }
}
