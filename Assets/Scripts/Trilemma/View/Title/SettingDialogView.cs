﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingDialogView : MonoBehaviour
{
    [SerializeField] private Slider _sliderBGM = default;
    [SerializeField] private Slider _sliderSE = default;

    public Action OnClickBack { get; set; }

    public void OnClickBackButton() {
        OnClickBack?.Invoke();
    }
    public void OnChangeValueBGM() {
        SoundManager.Instance.SetVolumeBGM(_sliderBGM.value / 100.0f / 5.0f);
    }

    public void OnChangeValueSE() {
        SoundManager.Instance.SetVolumeSE(_sliderSE.value / 100.0f);
    }
}
