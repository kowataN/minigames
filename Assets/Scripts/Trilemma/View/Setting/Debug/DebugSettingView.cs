﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugSettingView : SingletonMonoBhv <DebugSettingView> {
    [SerializeField] private Text _txtMessage1 = default;

    public void SetMesage1(string text) {
        _txtMessage1.text = text;
    }
}
