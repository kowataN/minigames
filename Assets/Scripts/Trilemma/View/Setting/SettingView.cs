﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class SettingView : MonoBehaviour {
    [SerializeField] private InputField _battleCount = default;
    [SerializeField] private Slider _slider = default;
    [SerializeField] private Button _btnMinus = default;
    [SerializeField] private Button _btnPlus = default;
    [SerializeField] private Button _btnBack = default;
    [SerializeField] private Button _btnStart = default;
    [SerializeField] private GameObject _panelWait = default;

    private void Awake() {
        Init();
    }

    public void Init() {
        _slider.value = BattleManager.Instance.BattleData.MaxTurn;
        UpdateBattleCount();
        _battleCount.text = BattleManager.Instance.BattleData.MaxTurn.ToString();

        var isMaster = PhotonManager.Instance.IsMasterClient;
        _battleCount.interactable = isMaster;
        _slider.interactable = isMaster;
        _btnBack.interactable = isMaster;
        _btnStart.interactable = isMaster;
        _btnMinus.interactable = isMaster;
        _btnPlus.interactable = isMaster;
        _panelWait.SetActive(!isMaster);

        _btnBack.gameObject.SetActive(GameManager.Instance.BattleMode == Entity.BattleMode.Solo);
    }

    private void SetPlusMinusButtonEnabled(bool value) {
        if (PhotonManager.Instance.IsMasterClient) {
            _btnMinus.interactable = value;
            _btnPlus.interactable = value;
        }

    }

    public void OnClickMinus() {
        SetPlusMinusButtonEnabled(false);
        _slider.value--;
    }

    public void OnClickPlus() {
        SetPlusMinusButtonEnabled(false);
        _slider.value++;
    }

    public void OnChangedSlider() {
        UpdateBattleCount();
        PhotonManager.Instance.SendBattleCount((int)_slider.value);
        SetPlusMinusButtonEnabled(true);
    }

    public void UpdateSlider(int value) {
        _slider.value = value;
        SetPlusMinusButtonEnabled(true);
    }

    private void UpdateBattleCount() {
        _battleCount.text = _slider.value.ToString();
    }

    public void OnClickStart() {
        GameManager.Instance.LockManager.Lock();

        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            TransitBattle();
        } else {
            PhotonManager.Instance.SendTransitBattle();
        }
    }

    public void TransitBattle() {
        BattleManager.Instance.BattleData.MaxTurn = (int)_slider.value;
        Fader.Instance.BlackOut(1.0f, () => {
            GameManager.Instance.LockManager.UnLock();
            GameManager.Instance.SetViewPosition(Entity.ViewType.Battle);
        });
    }

    public void OnClickBackButton() {
        GameManager.Instance.SetViewPosition(Entity.ViewType.Title);
    }
}
