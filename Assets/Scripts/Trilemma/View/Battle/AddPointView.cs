﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddPointView : MonoBehaviour
{
    [SerializeField] private Text _textPoint = default;
    public Text Point => _textPoint;

    public void SetPoint(int point) {
        _textPoint.text = ((point < 0) ? "" : "+") + point.ToString();
    }
}
