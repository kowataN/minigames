﻿using UnityEngine;
using UnityEngine.UI;

public class EmojiPartsView : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Button _button;

    public string ImageFile => _image.sprite.name;
    public Button Button => _button;

    public void Init(string fileName) {
    }
}
