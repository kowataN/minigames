﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecideView : MonoBehaviour
{
    [SerializeField] private Button _button = default;
    [SerializeField] private Text _text = default;

    public Button Button => _button;

    public void UpdateButtonText(string text) => _text.text = text;

    public void SetButtonEnabled(bool value)
    {
        _button.enabled = value;
        _text.color = value ? Color.black : Color.gray;
        UpdateButtonText("決定");
    }
}
