﻿using UnityEngine;
using UnityEngine.UI;

public class ReceiveStampView : MonoBehaviour
{
    [SerializeField] private Image _image = default;
    [SerializeField] private Animator _animator = default;

    public bool _isPlaying = false;

    public void PlayAnime(string fileName) {
        if (_isPlaying) {
            return;
        }

        Sprite[] sprites = ResourceManager.Instance.StempIconList;
        foreach (var sp in sprites) {
            if (sp.name == fileName) {
                _image.sprite = sp;
                break;
            }
        }

        _animator.Play("ReceiveStamp", 0, 0);
    }
}
