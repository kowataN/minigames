﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardView : MonoBehaviour
{
    [SerializeField] private GameObject _imageSurface = default;
    [SerializeField] private Image _cardImage = default;
    [SerializeField] private Text _cardName = default;

    public void SetActiveSurface(bool value)
    {
        _imageSurface.SetActive(value);
    }

    public void SetCardData(Entity.CardData cardData)
    {
        int index = cardData.SpriteIndex;
        _cardImage.sprite = ResourceManager.Instance.CardIconList[index];
        _cardName.text = cardData.Name;
    }
}
