﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointView : MonoBehaviour
{
    [SerializeField] private Text _textPoint = default;

    public void UpdatePoint(int value) => _textPoint.text = value.ToString() + "pt";
}
