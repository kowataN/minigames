﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugView : MonoBehaviour
{
    public static DebugView Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    [SerializeField] private Text _txtUserId = default;

    public void SetUserId(string userId) => _txtUserId.text = userId;

    #region 入力制御
    [Header("入力制御")]
    [SerializeField] private Button _buttonInput = default;
    [SerializeField] private Text _buttonInputText = default;

    private bool _IsLock = false;
    [SerializeField] bool _IsLockManager = default;

    public void OnClickInputButton()
    {
        _IsLock = _IsLock ? false : true;
        if (_IsLock)
        {
            GameManager.Instance.LockManager.Lock();
        }
        else
        {
            GameManager.Instance.LockManager.UnLock();
        }
        _IsLockManager = GameManager.Instance.LockManager.IsLock();

        _buttonInputText.text = "入力制御：" + (_IsLock ? "無効" : "有効");
    }
    #endregion

    #region ステート関連
    [Header("ステート関連")]
    [SerializeField] private Text _textMainState = default;
    [SerializeField] private Text _textSubState = default;

    [SerializeField] private Text _textEnemyMainState = default;
    [SerializeField] private Text _textEnemySubState = default;

    public void SetMainStateString(string stateName)
    {
        if (_textMainState == null)
        {
            return;
        }

        if (_textMainState.text != stateName)
        {
            Debug.Log("MAIN STATE: " + _textMainState.text + " → " + stateName);
        }
        _textMainState.text = stateName;
    }

    public void SetSubStateString(string stateName)
    {
        if (_textSubState == null)
        {
            return;
        }

        if (_textSubState.text != stateName)
        {
            Debug.Log(" SUB STATE: " + _textSubState.text + " → " + stateName);
        }
        _textSubState.text = stateName;
    }

    public void SetEnemyMainStateString(string stateName)
    {
        if (_textEnemyMainState == null)
        {
            return;
        }

        if (_textEnemyMainState.text != stateName)
        {
            Debug.Log("ENEMY MAIN STATE: " + _textEnemyMainState.text + " → " + stateName);
        }
        _textEnemyMainState.text = stateName;
    }

    public void SetEnemySubStateString(string stateName)
    {
        if (_textEnemySubState == null)
        {
            return;
        }

        if (_textEnemySubState.text != stateName)
        {
            Debug.Log("ENEMY  SUB STATE: " + _textEnemySubState.text + " → " + stateName);
        }
        _textEnemySubState.text = stateName;
    }
    #endregion

    #region 攻撃ログ
    [Header("攻撃ログ")]
    [SerializeField] private Text _textPlayerAttackLog = default;
    [SerializeField] private Text _textEnemyAttackLog = default;

    public void SetPlayerAttackLog(string text)
    {
        _textPlayerAttackLog.text = text;
    }
    public void SetEnemyAttackLog(string text)
    {
        _textEnemyAttackLog.text = text;
    }
    #endregion

    #region 設置カード
    [Header("相手設置カード")]
    [SerializeField] private Text[] _textEnemyFieldCard = default;

    public void SetEnemyFieldCard(int index, string text)
    {
        _textEnemyFieldCard[index].text = text;
    }
    #endregion

    #region ポイントアニメ
    [Header("ポイントアニメ")]
    [SerializeField] private Button _addPointButton = default;
    public void OnClickAddPointButton() {
        BattleManager.Instance.PlayerPresenter
            .AddPointPresenter.PlayAddPoint(Entity.AttackType.Attack, 2, () => { });
        BattleManager.Instance.PlayerPresenter
            .AddPointPresenter.PlayAddPoint(Entity.AttackType.Defense, -2, () => { });

        BattleManager.Instance.EnemyPresenter
            .AddPointPresenter.PlayAddPoint(Entity.AttackType.Attack, 2, () => { });
        BattleManager.Instance.EnemyPresenter
            .AddPointPresenter.PlayAddPoint(Entity.AttackType.Defense, -2, () => { });
    }

    #endregion

}
