﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextEffectView : MonoBehaviour
{
    [SerializeField] private Animator _animator = default;
    [SerializeField] private Text _message = default;

    [SerializeField] private bool _isEndAnimation = false;
    public bool IsEndAnimation => _isEndAnimation;

    public void PlayEffect()
    {
        _isEndAnimation = false;
        this.gameObject.SetActive(true);
        _animator.Play("TextAnimation", 0, 0);
    }

    public void PlayEffect(string msg)
    {
        _message.text = msg;
        PlayEffect();
    }
}
