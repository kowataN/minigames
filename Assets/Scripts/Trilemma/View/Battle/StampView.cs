﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StampView : MonoBehaviour
{
    [SerializeField] private Button _button = default;
    [SerializeField] private Animation _animation = default;
    [SerializeField] private GameObject _partsPanel = default;
    [SerializeField] private EmojiPartsView[] _emojiViews = default;

    [SerializeField] private List<AnimationClip> _animations = new List<AnimationClip>();

    public bool IsShow { get => _partsPanel.activeSelf; }

    public Button Button => _button;
    public EmojiPartsView[] EmojiViews => _emojiViews;


    private void Awake() {
        Debug.Log("StampView::Awake");
        _partsPanel.SetActive(false);
        foreach (AnimationState anim in _animation) {
            _animations.Add(anim.clip);
        }

        _emojiViews = _partsPanel.GetComponentsInChildren<EmojiPartsView>();
    }

    public void PlayShowAnime() {
        _partsPanel.SetActive(true);
        _animation.Play(_animations[0].name);
    }

    public void PlayHideAnime() {
        _partsPanel.SetActive(true);
        _animation.Play(_animations[1].name);
    }

    public void EndPlayHideAnime() {
        _partsPanel.SetActive(false);
    }
}
