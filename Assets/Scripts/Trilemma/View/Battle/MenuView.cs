﻿using UnityEngine;
using UnityEngine.UI;

public class MenuView : MonoBehaviour
{
    [SerializeField] private Button _button = default;

    public Button Button => _button;
}
