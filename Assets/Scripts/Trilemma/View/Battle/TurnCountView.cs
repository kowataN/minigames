﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class TurnCountView : MonoBehaviour
{
    [SerializeField] private Text _textCrtTurn = default;
    [SerializeField] private Text _textMaxTurn = default;

    public void Initialize(int maxTurn)
    {
        _textMaxTurn.text = "/" + maxTurn.ToString();
        UpdateCrtTurn(1);
    }

    public void UpdateCrtTurn(int value) => _textCrtTurn.text = value.ToString();
}
