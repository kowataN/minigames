﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldView : MonoBehaviour
{
    [SerializeField] private Entity.PlayerType _playerType = default;
    public Entity.PlayerType PlayerType => _playerType;

    [SerializeField] private HandView _handView = default;
    public HandView HandView => _handView;

    [SerializeField] private PointView _pointView = default;
    public PointView PointView => _pointView;

    [SerializeField] private DecideView _decideView = default;
    public DecideView DecideView => _decideView;

    [SerializeField] private CardDropView[] _cardDropView = default;
    public CardDropView[] CardDropView => _cardDropView;

    [SerializeField] private AddPointView[] _addPointView = default;
    public AddPointView[] AddPointView => _addPointView;

    [SerializeField] private StampView _stampView = default;
    public StampView StampView => _stampView;

    [SerializeField] private ReceiveStampView _receiveStampView = default;
    public ReceiveStampView ReceiveStampView => _receiveStampView;
}
