﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardDropView : MonoBehaviour
{
    [SerializeField] private GameObject _parent = default;
    public GameObject DropParent => _parent;
}
