﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandView : MonoBehaviour
{
    [SerializeField] private Transform _handTransform = default;
    public Transform HandTransform => _handTransform;

    public void CreateCard(CardPresenter cardPrefab, int[] ids)
    {
        foreach (var id in ids)
        {
            CardPresenter card = Instantiate(cardPrefab, _handTransform, false);
            card.Init(_handTransform.CompareTag("PlayerHand"), id);
        }
    }

    public int GetCardIndexFromCardId(int cardId) {
        var list = GetComponentsInChildren<CardPresenter>();
        for (int i=0; i<list.Length; i++) { 
            if (list[i].CardModel.CardData.Id == cardId) {
                return i;
            }
        }
        return -1;
    }
}
