﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchingView : MonoBehaviour
{
    [SerializeField] private MatchingManager _matchingManager= default;
    [SerializeField] private GameObject _searchPanel = default;
    [SerializeField] private InputField _roomName = default;
    [SerializeField] private Button _btnRandom = default;
    [SerializeField] private Button _btnFriend = default;
    [SerializeField] private Button _btnBack = default;
    [SerializeField] private Button _btnCancel = default; 
    [SerializeField] private Button _btnNext = default;
    [SerializeField] private Text _txtMatchMsg = default;
    [SerializeField] private Text _txtSystemMsg = default;

    private void Awake() {
        Init();
    }

    public void Init() {
        _searchPanel.SetActive(false);
        _txtMatchMsg.text = "対戦相手を探しています。";
        _txtSystemMsg.text = "しばらくお待ち下さい。";

        _btnFriend.interactable = false;
        _btnNext.interactable = false;
        _btnCancel.interactable = false;
        _roomName.text = "";

        _matchingManager.CallBackFoundPlayer.AddListener(FoundPlayer);
        _matchingManager.CallBackLeftPlayer.AddListener(LeftPlayer);
        _matchingManager.CallBackJoinRoom.AddListener(JoinRoom);
    }

    private void JoinRoom() {
        _btnCancel.interactable = true;
    }

    private void FoundPlayer() {
        _txtMatchMsg.text = "対戦相手が見つかりました。";
        _txtSystemMsg.text = PhotonManager.Instance.IsMasterClient
                                ? "あなたはホストです。" : "あなたはゲストです。";

        _btnNext.interactable = PhotonManager.Instance.IsMasterClient;
    }

    private void LeftPlayer() {
        _txtMatchMsg.text = "対戦相手を探しています。";
        _txtSystemMsg.text = "しばらくお待ち下さい。";

        _btnNext.interactable = false;
    }

    public void OnClickRandomMatchingButton() {
        _matchingManager.IsMatching = true;

        _btnFriend.interactable = false;
        _btnRandom.interactable = false;
        _btnBack.interactable = false;

        if (!PhotonManager.Instance.IsConnected) {
            _matchingManager.Connected(_roomName.text,
                MatchingManager.MatchMode.Random);
        } else {
            _matchingManager.RandomMatching();
        }

        _searchPanel.SetActive(true);
    }

    public void OnClickFriendButton() {
        _matchingManager.IsMatching = true;

        _btnFriend.interactable = false;
        _btnRandom.interactable = false;
        _btnBack.interactable = false;

        if (!PhotonManager.Instance.IsConnected) {
            _matchingManager.Connected(_roomName.text,
                MatchingManager.MatchMode.Friend);
        } else {
            _matchingManager.JoinOrCreateRoom(_roomName.text);
        }

        _searchPanel.SetActive(true);
    }

    /// <summary>
    /// 戻るボタン押下
    /// </summary>
    public void OnClickBackButton() {
        GameManager.Instance.SetViewPosition(Entity.ViewType.Title);
    }

    public void OnClickCancelButton() {
        _matchingManager.IsMatching = false;

        UpdateFriendButtonEnabled();
        _btnRandom.interactable = true;
        _btnBack.interactable = true;

        _txtMatchMsg.text = "対戦相手を探しています。";
        _txtSystemMsg.text = "しばらくお待ち下さい。";

        if (PhotonManager.Instance.IsConnected) {
            _matchingManager.LeaveRoom();
        }

        _searchPanel.SetActive(false);
    }

    public void OnClickNextButton() {
        GameManager.Instance.BattleMode = Entity.BattleMode.Multi;
        if (PhotonManager.Instance.IsMasterClient) {
            PhotonManager.Instance.SendSetSceneView(Entity.ViewType.Setting);
        }
    }

    public void OnValueChangedInput() {
        Debug.Log("OnValueChangedInput");
        UpdateFriendButtonEnabled();
    }

    public void OnEndEditInput() {
        Debug.Log("OnEndEditInput");
        UpdateFriendButtonEnabled();
    }

    private void UpdateFriendButtonEnabled() {
        _btnFriend.interactable = (_roomName.text.Length == 4);
    }
}
