﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugMatchingView : SingletonMonoBhv<DebugMatchingView>
{
    [SerializeField] Text _textMatchingMsg = default;
    [SerializeField] Text _textRoomName = default;

    public void SetMatchingMsg(string msg) {
        if (_textMatchingMsg) {
            _textMatchingMsg.text = msg;
        }
        //Debug.Log(msg);
    }

    public void SetRoomName(string name) {
        if (_textRoomName) {
            _textRoomName.text = "RoomName: " + name;
        }
        //Debug.Log(name);
    }
}
