﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Events;

public class MatchingManager : MonoBehaviourPunCallbacks {
    private string _roomName = default;
    private RoomOptions _roomOptions = new RoomOptions();
    private bool _isMatching = false;
    public enum MatchMode { 
        Friend, Random
    };
    private MatchMode _matchMode = MatchMode.Friend;

    public UnityEvent CallBackFoundPlayer;
    public UnityEvent CallBackLeftPlayer;
    public UnityEvent CallBackJoinRoom;

    public bool IsMatching {
        set { _isMatching = value; }
        get { return _isMatching; }
    }

    private void Start() {
        _roomOptions.MaxPlayers = 2;
        _roomOptions.PublishUserId = true;
    }

    public void Connected(string roomName, MatchMode mode) {
        _roomName = roomName;
        _matchMode = mode;

        PhotonNetwork.ConnectUsingSettings();
        if (string.IsNullOrEmpty(PhotonNetwork.NickName)) {
            PhotonNetwork.NickName = "Player_" + Random.Range(1, 999999);
        }
    }

    private void CreateRandomRoom(string roomName) {
        _roomOptions.IsVisible = true;
        PhotonNetwork.CreateRoom(roomName, _roomOptions, TypedLobby.Default);
    }

    public void JoinOrCreateRoom(string roomName) {
        _roomOptions.IsVisible = false;
        PhotonNetwork.JoinOrCreateRoom(roomName, _roomOptions, TypedLobby.Default);
    }

    public void RandomMatching() {
        _roomOptions.IsVisible = false;
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnConnectedToMaster() {
        DebugMatchingView.Instance.SetMatchingMsg("OnConnectedToMaster");
        if (_isMatching) {
            if (_matchMode == MatchMode.Random) {
                RandomMatching();
            } else {
                JoinOrCreateRoom(_roomName);
            }
        }
    }

    public void LeaveRoom() => PhotonNetwork.LeaveRoom();

    public override void OnJoinedRoom() {
        DebugMatchingView.Instance.SetMatchingMsg("OnJoinedRoom : " + PhotonNetwork.CurrentRoom.PlayerCount.ToString());
        DebugMatchingView.Instance.SetRoomName(PhotonNetwork.CurrentRoom.Name);
        //Debug.Log("MasterClient:" + PhotonManager.Instance.IsMasterClient.ToString());

        if (BattleManager.Instance) {
            BattleManager.Instance.UserId = PhotonNetwork.LocalPlayer.UserId;
        }

        CallBackJoinRoom?.Invoke();
        if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers) {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            Debug.Log("CurrentRoom.IsOpen:" + PhotonNetwork.CurrentRoom.IsOpen.ToString());

            CallBackFoundPlayer?.Invoke();
        }
    }

    public override void OnLeftRoom() {
        DebugMatchingView.Instance.SetMatchingMsg("OnLeftRoom");
        DebugMatchingView.Instance.SetRoomName("");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) {
        DebugMatchingView.Instance.SetMatchingMsg("OnPlayerEnteredRoom");

        if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers) {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            Debug.Log("CurrentRoom.IsOpen:" + PhotonNetwork.CurrentRoom.IsOpen.ToString());

            CallBackFoundPlayer?.Invoke();
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        DebugMatchingView.Instance.SetMatchingMsg("OnPlayerLeftRoom");
        PhotonNetwork.CurrentRoom.IsOpen = true;
        Debug.Log("CurrentRoom.IsOpen:" + PhotonNetwork.CurrentRoom.IsOpen.ToString());
        CallBackLeftPlayer?.Invoke();
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        DebugMatchingView.Instance.SetMatchingMsg("OnJoinRandomFailed");
        CreateRandomRoom(null);
    }

    public override void OnJoinRoomFailed(short returnCode, string message) {
        DebugMatchingView.Instance.SetMatchingMsg("OnJoinRoomFailed");
    }
}
