﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entity {
    /// <summary>
    /// プレイヤー種別
    /// </summary>
    public enum PlayerType {
        Player, Enemy
    }

    /// <summary>
    /// カードの種類
    /// </summary>
    public enum CardType {
        Gu, Choki, Pa
    }

    /// <summary>
    /// 決定ボタンの状態
    /// </summary>
    public enum DecideButtonState {
        Inactive,
        Active
    }

    /// <summary>
    /// 攻撃結果
    /// </summary>
    public enum AttackResult {
        None,
        Success,
        Failure,
        Draw,
    }

    /// <summary>
    /// バトル結果
    /// </summary>
    public enum BattleResult {
        Draw,
        Win,
        Lose,
    }

    public enum AttackType {
        Attack, Defense
    }

    /// <summary>
    /// 画面の種類
    /// </summary>
    public enum ViewType {
        Title, Matching, Setting, Battle,
    }

    /// <summary>
    /// バトルモード
    /// </summary>
    public enum BattleMode {
        Solo, Multi,
    }

    /// <summary>
    /// バトル状態
    /// </summary>
    public enum BattleState {
        None, InBattle,
    }
}
