﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Entity
{
    [CreateAssetMenu(fileName = "CardData", menuName = "Entity/CreateCard")]
    public class CardData : ScriptableObject
    {
        public int Id = 1;
        public string Name = "カード名";

        public CardType CardType = CardType.Gu;
        public int SpriteIndex = default;

        public CardType LoseCardType = CardType.Pa;
    }
}
