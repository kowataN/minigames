﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entity
{
    [CreateAssetMenu(fileName = "BattleData", menuName = "Entity/CreateBattleData")]
    public class BattleData : ScriptableObject
    {
        public int MaxTurn = 5;
        public int MaxPoint = 100;
        public int InitialPoint = 10;
        public int FloatingPoint = 2;
    }
}
