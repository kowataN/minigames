﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardDrop: MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        CardMovement card = eventData.pointerDrag.GetComponent<CardMovement>();
        if (card != null)
        {
            if (this.gameObject.tag == "PlayerParent" && this.transform.childCount != 0)
            {
                return;
            }
            card.ParentTransform = this.transform;

            SoundManager.Instance.PlaySetCard();
        }
    }
}
