﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardMovement : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public Transform ParentTransform = default;
    private CanvasGroup _canvasGroup = default;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (GameManager.Instance.LockManager.IsLock())
        {
            Debug.Log("入力ロック中");
            return;
        }

        SoundManager.Instance.PlayMoveCard();

        ParentTransform = transform.parent;
        transform.SetParent(transform.parent.parent, false);
        _canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (GameManager.Instance.LockManager.IsLock())
        {
            return;
        }

        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (GameManager.Instance.LockManager.IsLock())
        {
            return;
        }

        transform.SetParent(ParentTransform);
        _canvasGroup.blocksRaycasts = true;
    }
}
