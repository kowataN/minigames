﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class BattleModel
{
    private Entity.BattleData _battleData = default;
    public Entity.BattleData BattleData => _battleData;

    private IntReactiveProperty _crtTurn = default;
    public IntReactiveProperty CrtTurn => _crtTurn;

    private PlayerModel _playerModel = default;
    public PlayerModel PlayerModel => _playerModel;

    private PlayerModel _enemyModel = default;
    public PlayerModel EnemyModel => _enemyModel;

    public bool GotoTitle { get; set; }


    public BattleModel(string name)
    {
        _battleData = Resources.Load<Entity.BattleData>("Trilemma/BattleData/" + name);
        if (!_battleData)
        {
            Debug.LogError("_BattleData is null");
            return;
        }

        _crtTurn = new IntReactiveProperty(1);

        _playerModel = new PlayerModel();
        _enemyModel = new PlayerModel();
    }

    public Entity.BattleResult IsPlayerWin()
    {
        if (_playerModel.Point.Value == _enemyModel.Point.Value)
        {
            return Entity.BattleResult.Draw;
        }
        else
        {
            return (_playerModel.Point.Value > _enemyModel.Point.Value)
                ? Entity.BattleResult.Win
                : Entity.BattleResult.Lose;
        }
    }
}
