﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardModel
{
    private Entity.CardData _cardData = default;
    public Entity.CardData CardData
    {
        get { return _cardData; }
    }

    public CardModel(int id)
    {
        _cardData = Resources.Load<Entity.CardData>("Trilemma/CardData/CardData" + id.ToString());
    }
}
