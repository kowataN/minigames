﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PlayerModel
{
    private CardModel _leftCardModel = default;
    private CardModel _rightCardModel = default;

    private BoolReactiveProperty _isDecide = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsDecide => _isDecide;

    private BoolReactiveProperty _decideButtonActive = new BoolReactiveProperty(false);
    public BoolReactiveProperty DecideButtonActive => _decideButtonActive;

    private IntReactiveProperty _point = new IntReactiveProperty(0);
    public IntReactiveProperty Point => _point;

    private Entity.AttackResult _attackResult = Entity.AttackResult.None;
    public Entity.AttackResult AttackResult => _attackResult;


    public void Initialize()
    {
        _isDecide.Value = false;
        _decideButtonActive.Value = false;
    }

    public void SetLeftCardModel(CardModel card) => _leftCardModel = card;
    public void SetRightCardModel(CardModel card) => _rightCardModel = card;

    public void SetAttackResult(Entity.AttackResult result) => _attackResult = result;
}
