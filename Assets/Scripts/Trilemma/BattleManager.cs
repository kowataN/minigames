﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;

public class BattleManager : SingletonMonoBhv<BattleManager> {
    [SerializeField] CardPresenter _cardPrefab = default;
    [SerializeField] private TurnCountView _turnCountView = default;
    [SerializeField] private TextEffectView _textEffectView = default;
    [SerializeField] private FieldView _playerFieldView = default;
    [SerializeField] private FieldView _enemyFieldView = default;
    [SerializeField] private MenuView _menuView = default;

    private BattleModel _battleModel = null;
    public Entity.BattleData BattleData { get { return _battleModel.BattleData; } }

    public TurnCountView TurnCountView => _turnCountView;
    public TextEffectView TextEffectView => _textEffectView;
    public FieldView PlayerFieldView => _playerFieldView;
    public FieldView EnemyFieldView => _enemyFieldView;

    private TurnCountPresenter _turnCountPresenter = default;
    private PlayerPresenter _playerPresenter = default;
    public PlayerPresenter PlayerPresenter => _playerPresenter;
    private PlayerPresenter _enemyPresenter = default;
    public PlayerPresenter EnemyPresenter => _enemyPresenter;

    private TextEffectPresenter _textEffectPresenter = default;
    private MenuPresenter _menuPresenter = default;

    private Entity.BattleState _battleState = Entity.BattleState.None;

    private bool _isCreate = false;

    /// <summary>
    /// Photonから得られるユーザーID
    /// </summary>
    public string UserId { get; set; }

    public bool GotoTitle {
        get { return _battleModel.GotoTitle; }
        set { _battleModel.GotoTitle = value; }
    }

    public void BattleStart() {
        _battleState = Entity.BattleState.InBattle;
    }

    public void BattleEnd() {
        _battleState = Entity.BattleState.None;
    }

    public void LoadBattleData() {
        _battleModel = new BattleModel("BattleData");
    }

    public void Create() {
        Debug.Log("BattleManager::Create  Mode:" + GameManager.Instance.BattleMode.ToString());

        if (_isCreate == false) {
            if (_battleModel == null) {
                LoadBattleData();
            }

            DebugView.Instance?.SetUserId(UserId);

            _turnCountPresenter = new TurnCountPresenter(_battleModel, _turnCountView);
            _playerPresenter = new PlayerPresenter(_battleModel.PlayerModel, _playerFieldView);
            _playerPresenter.Init();
            if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                _enemyPresenter = new EnemyPresenter(_battleModel.EnemyModel, _enemyFieldView);
                _enemyPresenter.Init();
            } else {
                _enemyPresenter = new MultiPlayerPresenter(_battleModel.EnemyModel, _enemyFieldView);
                ((MultiPlayerPresenter)_enemyPresenter).Init();
            }
            _menuPresenter = new MenuPresenter(_menuView);
            _textEffectPresenter = new TextEffectPresenter(_textEffectView);

            int[] hostCards = new int[] { 1, 2, 3 };
            int[] guestCards = new int[] { 2, 3, 1 };
            PhotonManager.Instance.SendCreateHandCards(hostCards, guestCards);

            this.UpdateAsObservable()
                .Subscribe(_ => {
                    if (_battleState == Entity.BattleState.InBattle) {
                        StateManager.Instance.Execute();
                    }
                })
                .AddTo(this.gameObject);
        }

        StateManager.Instance.Initialize();
        _isCreate = true;
    }
    
    public void CreateHandCard(int[] hostCards, int[] guestCards) {
        if (PhotonManager.Instance.IsMasterClient) {
            _playerPresenter.CreateHand(_cardPrefab, hostCards);
            _enemyPresenter.CreateHand(_cardPrefab, guestCards);
        } else {
            _playerPresenter.CreateHand(_cardPrefab, guestCards);
            _enemyPresenter.CreateHand(_cardPrefab, hostCards);

        }
    }

    public void Initialize() {
        _playerPresenter.InitPoint(_battleModel.BattleData.InitialPoint);
        _enemyPresenter.InitPoint(_battleModel.BattleData.InitialPoint);

        InitHandCardPosition();
        InitDecideButton();
        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            ((EnemyPresenter)EnemyPresenter).InitThink();
        }

        _menuPresenter.HideMenu();

        _battleModel.CrtTurn.Value = 1;
        _battleModel.GotoTitle = false;
    }

    public bool IsBothDecided() {
        return _battleModel.PlayerModel.IsDecide.Value && _battleModel.EnemyModel.IsDecide.Value;
    }

    public void InitDecideButton() {
        _playerPresenter.DecidePresenter.Initialize();
        _enemyPresenter.DecidePresenter.Initialize();
    }

    public bool IsLastTurn() {
        return _battleModel.CrtTurn.Value == _battleModel.BattleData.MaxTurn;
    }

    public void AddCurrentTurn() {
        _battleModel.CrtTurn.Value++;
    }

    public void InitHandCardPosition() {
        _playerPresenter.InitHandCardPosition();
        _enemyPresenter.InitHandCardPosition();

        _enemyPresenter.HandPresenter.ForceCloseCard();
    }

    public void PlayGameStartEffect() {
        _textEffectPresenter.PlayEffect("ゲーム開始");
    }

    public void PlayStartTurnEffect() {
        _textEffectPresenter.PlayEffect(_battleModel.CrtTurn.ToString() + " ターン目");
    }

    public void PlayOpenCardEffect() {
        _textEffectPresenter.PlayEffect("カードオープン");
    }

    public void PlayGameEndEffect() {
        _textEffectPresenter.PlayEffect("ゲーム終了");
    }

    public void PlayBattleResultEffect() {
        var res = _battleModel.IsPlayerWin();
        var resStr = "引き分けです。";
        if (res == Entity.BattleResult.Win) {
            resStr = "あなたの勝ちです。";
        } else if (res == Entity.BattleResult.Lose) {
            resStr = "あなたの負けです。";
        }
        _textEffectPresenter.PlayEffect(resStr);
    }

    public bool IsEndEffect() {
        return _textEffectPresenter.IsEndEffect();
    }

    public void ShowMenu() {
        _menuPresenter.ShowMenu();
    }
}