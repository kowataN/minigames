﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiPlayerPresenter : PlayerPresenter
{
    private ReceiveStampPresenter _receiveStampPresenter = default;

    public MultiPlayerPresenter(PlayerModel model, FieldView view) : base(model, view) {
        Debug.Log("MultiPlayerPresenter::MultiPlayerPresenter");

        _receiveStampPresenter = new ReceiveStampPresenter(view.ReceiveStampView);
    }

    public override void Init() {
        Debug.Log("MultiPlayerPresenter::Init");
        _decidePresenter.SetObservable(false);
        _decidePresenter.Initialize();
        _pointPresenter.SetObservable();
    }

    public void TurnOverCard(bool isShowSurface, TweenCallback callback) {
        CardDropPresenter.TurnOverCard(isShowSurface, callback);
    }

    public void MoveHandCard(Entity.PlayerType playerType, Entity.AttackType atkType, int cardId) {
        int handIndex = HandPresenter.GetCardIndex(cardId);
        int dropIndex = atkType == Entity.AttackType.Attack ? 0 : 1;
        Debug.Log(string.Format("Player[{0}] AtkType[{1}] Drop[{2}] CardID[{3}] Hand[{4}]",
            playerType, dropIndex, atkType, cardId, handIndex));
        HandPresenter.MoveHandCard(handIndex, dropIndex, View, null);
    }

    public void PlayStampAnime(string fileName) {
        Debug.Log("EnemyPresenter::PlayStampAnime：" + fileName);
        _receiveStampPresenter.PlayAnime(fileName);
    }
}
