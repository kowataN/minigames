﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PointPresenter : IPresenter
{
    private PlayerModel _model = default;
    private PointView _view = default;

    public PointPresenter(PlayerModel model, PointView view)
    {
        _model = model;
        _view = view;
    }

    public void SetObservable()
    {
        _model.Point
            .ObserveEveryValueChanged(x => x.Value)
            .Subscribe(x => _view.UpdatePoint(x))
            .AddTo(_view.gameObject);
    }

    public void InitPoint(int point) => _model.Point.Value = point;
    public void AddPoint(int point) => _model.Point.Value += point;
}
