﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;

public class PlayerPresenter : IPresenter
{
    private PlayerModel _model = default;

    private FieldView _view = default;
    public FieldView View => _view;

    private HandPresenter _handPresenter = default;
    public HandPresenter HandPresenter => _handPresenter;

    protected CardDropPresenter _cardDropPresenter = default;
    public CardDropPresenter CardDropPresenter => _cardDropPresenter;

    protected DecidePresenter _decidePresenter = default;
    public DecidePresenter DecidePresenter => _decidePresenter;

    protected PointPresenter _pointPresenter = default;
    public PointPresenter PointPresenter => _pointPresenter;

    private AddPointPresenter _addPointPresenter = default;
    public AddPointPresenter AddPointPresenter => _addPointPresenter;

    private StampPresenter? _stampPresenter = default;
    public StampPresenter? StampPresenter => _stampPresenter;

    public PlayerPresenter(PlayerModel model, FieldView view)
    {
        _model = model;
        _view = view;

        _cardDropPresenter = new CardDropPresenter(model, view);
        //_cardDropPresenter.SetObservable();

        _decidePresenter = new DecidePresenter(model, view.DecideView);
        //_decidePresenter.SetObservable();
        //_decidePresenter.Initialize();

        _pointPresenter = new PointPresenter(model, view.PointView);
        //_pointPresenter.SetObservable();

        _addPointPresenter = new AddPointPresenter(view.AddPointView, view.PlayerType);

        _stampPresenter = new StampPresenter(view.StampView);

        SetHandPresenter(new HandPresenter(_view.HandView));
    }

    public virtual void Init() {
        _cardDropPresenter.SetObservable();
        _decidePresenter.SetObservable(true);
        _decidePresenter.Initialize();
        _pointPresenter.SetObservable();
        _stampPresenter.SetObservable();
    }

    public void SetHandPresenter(HandPresenter presenter) => _handPresenter = presenter;

    public void CreateHand(CardPresenter cardPrefab, int[] ids) => _handPresenter.CreateCard(cardPrefab, ids);

    public void InitPoint(int point) => _model.Point.Value = point;

    public Entity.AttackResult Attack(PlayerPresenter target)
    {
        // 対象の防御カード取得
        var targetCardPresenter = target._cardDropPresenter.GetCardPresenter(1);
        var targetCard = targetCardPresenter.CardModel.CardData;

        // 自身の攻撃カード取得
        var cardPresenter = _cardDropPresenter.GetCardPresenter(0);
        var card = cardPresenter.CardModel.CardData;

        if (card.CardType == targetCard.CardType)
        {
            return Entity.AttackResult.Draw;
        }
        return card.CardType == targetCard.LoseCardType
            ? Entity.AttackResult.Success
            : Entity.AttackResult.Failure;
    }

    public void SetAttackResult(Entity.AttackResult result) => _model.SetAttackResult(result);
    public Entity.AttackResult GetAttackResult()
    {
        return _model.AttackResult;
    }

    public void AddPoint(int point) => _pointPresenter.AddPoint(point);

    public void InitHandCardPosition()
    {
        CardDropPresenter.MoveDropCard(_view);
    }
}
