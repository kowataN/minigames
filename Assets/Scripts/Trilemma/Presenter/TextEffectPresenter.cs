﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextEffectPresenter : IPresenter
{
    private TextEffectView _view = default;

    public TextEffectPresenter(TextEffectView view) => _view = view;


    public void PlayEffect(string message)
    {
        _view.PlayEffect(message);
    }

    public bool IsEndEffect()
    {
        return _view.IsEndAnimation;
    }
}
