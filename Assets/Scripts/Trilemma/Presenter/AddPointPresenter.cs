﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AddPointPresenter : IPresenter {
    private AddPointView[] _view = default;
    private Entity.PlayerType _playerType = default;

    public AddPointPresenter(AddPointView[] view, Entity.PlayerType playerType) {
        _view = view;
        _playerType = playerType;
    }

    public void PlayAddPoint(Entity.AttackType type, int point, TweenCallback callback) {
        int nType = (int)type;
        _view[nType].SetPoint(point);

        float plusPos = _playerType == Entity.PlayerType.Player ? 80.0f : -80.0f;
        float minusPos = plusPos * (-1.0f);

        var seq = DOTween.Sequence();
        Color targetColor = point < 0 ? Color.red : Color.green;
        seq.Append(_view[nType].Point.DOColor(targetColor, 0.5f))
            .Join(_view[nType].transform.DOLocalMoveY(plusPos, 0.5f).SetRelative(true))
            .AppendInterval(1.0f)
            .Append(_view[nType].Point.DOColor(Color.clear, 0.3f))
            .Join(_view[nType].transform.DOLocalMoveY(minusPos, 0.3f).SetRelative(true))
            .OnComplete(() => {
                callback?.Invoke();
            })
            .Play();
    }
}
