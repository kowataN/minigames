﻿using UnityEngine;

public class ReceiveStampPresenter : IPresenter
{
    private ReceiveStampView _view = default;

    public ReceiveStampPresenter(ReceiveStampView view) => _view = view;

    public void PlayAnime(string fileName) {
        Debug.Log("ReceiveStampPresenter::PlayAnime：" + fileName);
        _view.PlayAnime(fileName);
    }
}
