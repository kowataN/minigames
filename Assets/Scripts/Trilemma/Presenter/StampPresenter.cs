﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class StampPresenter : IPresenter
{
    private StampView _view = default;

    public StampPresenter(StampView view) => _view = view;

    public void SetObservable() {
        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            _view.Button.interactable = false;
            _view.gameObject.SetActive(false);
            return;
        }

        Debug.Log("StampPresenter::SetObservable");
        _view.Button.OnClickAsObservable()
            .Subscribe(_ => {
                Debug.Log("スタンプボタン押下");
                SoundManager.Instance.PlayTap();
                if (_view.IsShow) {
                    _view.PlayHideAnime();
                } else {
                    _view.PlayShowAnime();
                }
            })
            .AddTo(_view.gameObject);

        foreach (var view in _view.EmojiViews) {
            view.Button.OnClickAsObservable()
                .Subscribe(_ => {
                    Debug.Log("スタンプ押下：" + view.ImageFile);
                    SoundManager.Instance.PlayTap();
                    PhotonManager.Instance.SendStamp(view.ImageFile);
                })
                .AddTo(_view.gameObject);
        }
    }
}
