﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class MenuPresenter : IPresenter
{
    private MenuView _view = default;

    public MenuPresenter(MenuView view)
    {
        _view = view;
        _view.gameObject.SetActive(false);

        SetObservable();
    }

    private void SetObservable()
    {
        _view.Button.OnClickAsObservable()
            .Subscribe(_ => {
                if (BattleManager.Instance.GotoTitle)
                {
                    return;
                }
                BattleManager.Instance.GotoTitle = true;
            })
            .AddTo(_view);
    }

    public void ShowMenu() => _view.gameObject.SetActive(true);
    public void HideMenu() => _view.gameObject.SetActive(false);
}
