﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class DecidePresenter : IPresenter {
    private PlayerModel _model = default;
    public PlayerModel Model { get; }

    private DecideView _view = default;
    public DecideView View { get; }

    public DecidePresenter(PlayerModel model, DecideView view)
    {
        _model = model;
        _view = view;
    }

    public void Initialize()
    {
        _model.Initialize();
        _view.SetButtonEnabled(false);
    }

    public virtual void SetObservable(bool setOnClick)
    {
        _model.DecideButtonActive
            .ObserveEveryValueChanged(x => x.Value)
            .Subscribe(x => {
                _view.SetButtonEnabled(x);
            })
            .AddTo(_view.gameObject);

        if (setOnClick) {
            _view.Button.OnClickAsObservable()
                .Subscribe(_ => {
                    ExecDecicde();
                    PhotonManager.Instance.SendDecidePressed();
                })
                .AddTo(_view.gameObject);
        }
    }

    public void OnClickDecide()
    {
        ExecDecicde();
    }

    private void ExecDecicde() {
        _model.IsDecide.Value = true;
        _view.UpdateButtonText("決定済み");
        SoundManager.Instance.PlayTap();
    }
}
