﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPresenter : MonoBehaviour
{
    private CardModel _model = default;
    public CardModel CardModel => _model;

    [SerializeField] private CardView _view = default;

    public void Init(bool isPlayer, int id)
    {
        _model = new CardModel(id);

        _view.SetCardData(_model.CardData);
        SetActiveSurface(isPlayer);
    }

    public void SetActiveSurface(bool value)
    {
        _view.SetActiveSurface(value);
    }
}
