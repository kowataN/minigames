﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class TurnCountPresenter
{
    private BattleModel _model = default;
    private TurnCountView _view = default;

    public TurnCountPresenter(BattleModel model, TurnCountView view)
    {
        _model = model;
        _view = view;

        _view.Initialize(_model.BattleData.MaxTurn);
        SetObservable();
    }

    public void SetObservable()
    {
        // ターン数に変動があったら画面を更新
        _model.CrtTurn
            .ObserveEveryValueChanged(x => x.Value)
            .Subscribe(x => _view.UpdateCrtTurn(x))
            .AddTo(_view.gameObject);
    }
}
