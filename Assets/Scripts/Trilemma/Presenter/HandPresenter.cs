﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HandPresenter : IPresenter {
    private HandView _view = default;

    /// <summary>
    /// 相手用フィールド座標
    /// </summary>
    private readonly Vector2[] _fields = { new Vector2(715.0f, -600.0f), new Vector2(360.0f, -600.0f) };

    public HandPresenter(HandView view) => _view = view;

    public int GetCardIndex(int cardId) {
        return _view.GetCardIndexFromCardId(cardId);
    }

    public void CreateCard(CardPresenter cardPrefab, int[] ids) => _view.CreateCard(cardPrefab, ids);

    /// <summary>
    /// 手札のカード枚数を返します。
    /// </summary>
    /// <returns></returns>
    public int GetCardCount() => _view.gameObject.transform.childCount;

    /// <summary>
    /// 相手用カード移動処理
    /// </summary>
    /// <param name="index"></param>
    /// <param name="dropIndex"></param>
    /// <param name="fieldView"></param>
    /// <param name="callback"></param>
    public void MoveHandCard(int index, int dropIndex, FieldView fieldView, TweenCallback? callback) {
        Transform card = _view.gameObject.transform.GetChild(index);
        if (card) {
            CardPresenter cardPre = card.GetComponent<CardPresenter>();
            DebugView.Instance?.SetEnemyFieldCard(dropIndex, cardPre.CardModel.CardData.Name);

            card.parent = fieldView.gameObject.transform;

            var seq = DOTween.Sequence();
            seq.Append(card.DOLocalMove(_fields[dropIndex], 1.0f));
            seq.AppendCallback(() => {
                card.parent = fieldView.CardDropView[dropIndex].transform;
                callback?.Invoke();
            });
        }
    }


    public void ForceCloseCard() {
        int cardCount = _view.gameObject.transform.childCount;
        for (int i = 0; i < cardCount; i++) {
            var card = _view.gameObject.transform.GetChild(i);
            CardPresenter cardPre = card.GetComponent<CardPresenter>();
            if (cardPre == null) {
                continue;
            }

            cardPre.SetActiveSurface(false);
        }
    }
}
