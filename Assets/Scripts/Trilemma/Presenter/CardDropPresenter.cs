﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;

public class CardDropPresenter : IPresenter
{
    private PlayerModel _model = default;
    private CardDropView[] _dropView = default;

    private bool _leftSet = false;
    private bool _rightSet = false;


    public CardDropPresenter(PlayerModel model, FieldView view)
    {
        _model = model;
        _dropView = view.CardDropView;
    }

    public void SetObservable()
    {
        _dropView[0].UpdateAsObservable()
            .Subscribe(x =>
            {
                if (_leftSet)
                {
                    if (_dropView[0].gameObject.transform.childCount == 0)
                    {
                        _model.SetLeftCardModel(null);
                        _leftSet = false;
                        _model.DecideButtonActive.Value = false;
                    }
                    return;
                }

                if (_dropView[0].gameObject.transform.childCount > 0)
                {
                    var cardPresenter = _dropView[0].GetComponentInChildren<CardPresenter>();
                    _model.SetLeftCardModel(cardPresenter.CardModel);
                    Debug.Log("左に" + cardPresenter.CardModel.CardData.Name + "を配置");
                    _leftSet = true;

                    if (GameManager.Instance.BattleMode == Entity.BattleMode.Multi) {
                        Entity.AttackType atkType = _dropView[0].name.Contains("Atk") 
                            ? Entity.AttackType.Attack : Entity.AttackType.Defense;
                        PhotonManager.Instance.SendMoveCard(atkType, cardPresenter.CardModel.CardData.Id);
                    }

                    if (_rightSet)
                    {
                        _model.DecideButtonActive.Value = true;
                    }
                }
            }).AddTo(_dropView[0].gameObject);

        _dropView[1].UpdateAsObservable()
            .Subscribe(x =>
            {
                if (_rightSet)
                {
                    if (_dropView[1].gameObject.transform.childCount == 0)
                    {
                        _model.SetRightCardModel(null);
                        Debug.Log("右のカードを移動");
                        _rightSet = false;
                        _model.DecideButtonActive.Value = false;
                    }
                    return;
                }

                if (_dropView[1].gameObject.transform.childCount > 0)
                {
                    var cardPresenter = _dropView[1].GetComponentInChildren<CardPresenter>();
                    _model.SetRightCardModel(cardPresenter.CardModel);

                    Debug.Log("右に" + cardPresenter.CardModel.CardData.Name + "を配置");
                    _rightSet = true;

                    if (GameManager.Instance.BattleMode == Entity.BattleMode.Multi) {
                        Entity.AttackType atkType = _dropView[1].name.Contains("Atk")
                            ? Entity.AttackType.Attack : Entity.AttackType.Defense;
                        PhotonManager.Instance.SendMoveCard(atkType, cardPresenter.CardModel.CardData.Id);
                    }

                    if (_leftSet)
                    {
                        _model.DecideButtonActive.Value = true;
                    }
                }
            }).AddTo(_dropView[1].gameObject);
    }

    /// <summary>
    /// 子が空いているインデックスを返します。
    /// </summary>
    /// <returns></returns>
    public int GetEmptyChildIndex()
    {
        for (int i=0; i< _dropView.Length; i++)
        {
            if (_dropView[i].transform.childCount == 0)
            {
                return i;
            }
        }

        // 空きなし
        return -1;
    }

    public void TurnOverCard(bool isShowSurface, TweenCallback callback)
    {
        Transform[] card = {
            _dropView[0].gameObject.transform.GetChild(0),
            _dropView[1].gameObject.transform.GetChild(0)
        };

        CardPresenter[] presenter = {
            card[0].GetComponentInChildren<CardPresenter>(),
            card[1].GetComponentInChildren<CardPresenter>()
        };

        Sequence[] seqList = { DOTween.Sequence(), DOTween.Sequence() };
        {
            seqList[0].InsertCallback(0.0f, () =>  SoundManager.Instance.PlayTurnCard() );
            seqList[0].Append(card[0].DOScaleX(0.0f, 0.5f));
            seqList[0].AppendCallback(() =>
            {                
                var card = _dropView[0].gameObject.transform.GetChild(0);
                var presenter = card.GetComponentInChildren<CardPresenter>();
                presenter.SetActiveSurface(isShowSurface);
            });
            seqList[0].Append(card[0].DOScaleX(1.0f, 0.5f));
        }
        {
            seqList[1].InsertCallback(0.0f, () => SoundManager.Instance.PlayTurnCard());
            seqList[1].Append(card[1].DOScaleX(0.0f, 0.5f));
            seqList[1].AppendCallback(() =>
            {
                SoundManager.Instance.PlayTurnCard();
                var card = _dropView[1].gameObject.transform.GetChild(0);
                var presenter = card.GetComponentInChildren<CardPresenter>();
                presenter.SetActiveSurface(isShowSurface);
            });
            seqList[1].Append(card[1].DOScaleX(1.0f, 0.5f));
        }

        var seq = DOTween.Sequence();
        if (isShowSurface == false)
        {
            seq.SetDelay(1.0f);
        }
        seq.Append(seqList[0])
            .Join(seqList[1])
            .AppendCallback(callback);
    }

    public CardPresenter GetCardPresenter(int index)
    {
        var card = _dropView[index].gameObject.transform.GetChild(0);
        return card.GetComponentInChildren<CardPresenter>();
    }

    public void MoveDropCard(FieldView fieldView)
    {
        for (int i=0; i< _dropView.Length; i++)
        {
            if (_dropView[i].gameObject.transform.childCount == 0)
            {
                continue;
            }

            Transform card = _dropView[i].gameObject.transform.GetChild(0);
            if (card)
            {
                CardPresenter cardPre = card.GetComponent<CardPresenter>();

                card.parent = fieldView.HandView.HandTransform;
            }
        }
    }
}
