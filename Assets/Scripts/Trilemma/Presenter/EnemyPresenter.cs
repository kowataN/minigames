﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPresenter : PlayerPresenter
{
    private Thinker _thinker = default;

    public EnemyPresenter(PlayerModel model, FieldView view) : base(model, view)
    {
        _thinker = new Thinker(model, view);
        InitThink();
    }

    public override void Init() {
        _cardDropPresenter.SetObservable();
        _decidePresenter.SetObservable(true);
        _decidePresenter.Initialize();
        _pointPresenter.SetObservable();
    }

    public void InitThink() => _thinker.Init();

    public void Think() => _thinker.Think();

    public void TurnOverCard(bool isShowSurface, TweenCallback callback) {
        CardDropPresenter.TurnOverCard(isShowSurface, callback);
    }

    public void MoveHandCard(int handIndex, int DropIndex, TweenCallback callback) {
        HandPresenter.MoveHandCard(handIndex, DropIndex, View, callback);
    }
}
