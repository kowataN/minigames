﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonMonoBhv<SoundManager>
{
    [SerializeField] AudioClip _moveCard = default;
    [SerializeField] AudioClip _setCard = default;
    [SerializeField] AudioClip _turnCard = default;

    [SerializeField] AudioClip _Tap = default;
    [SerializeField] AudioClip _Cancel = default;

    [SerializeField] private AudioSource _asBGM = default;
    [SerializeField] private AudioSource _asSE = default;

    private float _backupSEVolume = 0.0f;
    private float _backupBGMVolume = 0.0f;

    public void PlayMoveCard() => _asSE.PlayOneShot(_moveCard);
    public void PlaySetCard() => _asSE.PlayOneShot(_setCard);
    public void PlayTurnCard() => _asSE.PlayOneShot(_turnCard);

    public void PlayTap() => _asSE.PlayOneShot(_Tap);
    public void PlayCancel() => _asSE.PlayOneShot(_Cancel);


    public void SetVolumeBGM(float vol) => _asBGM.volume = vol;

    public void SetVolumeSE(float vol) => _asSE.volume = vol;

    public void BackupVolume()
    {
        _backupSEVolume = _asSE.volume;
        _backupBGMVolume = _asBGM.volume;
    }

    public void RevertBckupData()
    {
        _asBGM.volume = _backupBGMVolume;
        _asSE.volume = _backupSEVolume;
    }

    /// <summary>
    /// 現在のBGMの音量を返します。
    /// </summary>
    /// <returns>BGM音量</returns>
    public float GetBGMVolume() => _asBGM.volume;

    /// <summary>
    /// 現在のSEの音量を返します。
    /// </summary>
    /// <returns>SE音量</returns>
    public float GetSEVolume() => _asSE.volume;
}
