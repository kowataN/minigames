﻿using UnityEngine;
using Photon.Pun;

public class PhotonManager : SingletonMonoBhv<PhotonManager> {
    [SerializeField] private PhotonView _photonView = default;
    [SerializeField] private SettingView _settingView = default;

    public bool IsConnected {
        get {
            if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                return true;
            }
            return PhotonNetwork.IsConnected;
        }
    }

    public bool IsMasterClient {
        get {
            if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
                return true;
            }

            if (!IsConnected) {
                return false;
            }

            return PhotonNetwork.IsMasterClient;
        }
    }

    public void SendSetSceneView(Entity.ViewType type) {
        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            PunSetSceneView(type);
            return;
        }
        _photonView.RPC("PunSetSceneView", RpcTarget.AllViaServer, type);
    }

    [PunRPC]
    private void PunSetSceneView(Entity.ViewType type) {
        GameManager.Instance.SetViewPosition(type);
    }

    public void SendBattleCount(int value) {
        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            PunBattleCount(value);
            return;
        }
        if (IsMasterClient) {
            _photonView.RPC("PunBattleCount", RpcTarget.Others, value);
        }
    }

    [PunRPC]
    private void PunBattleCount(int value) {
        _settingView.UpdateSlider(value);
    }

    public void SendTransitBattle() {
        _photonView.RPC("PunTransitBattle", RpcTarget.AllViaServer);
    }

    [PunRPC]
    private void PunTransitBattle() {
        _settingView.TransitBattle();
    }

    public void SendCreateHandCards(int[] hostCards, int[] GuestCards) {
        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            BattleManager.Instance.CreateHandCard(hostCards, GuestCards);
            return;
        }

        if (PhotonManager.Instance.IsMasterClient) {
            _photonView.RPC("PunCreateHandCards", RpcTarget.AllViaServer, hostCards, GuestCards);
        }
    }

    [PunRPC]
    private void PunCreateHandCards(int[] hostCards, int[]GuestCards) {
        //Debug.Log(hostCards.ToString());
        //Debug.Log(GuestCards.ToString());
        BattleManager.Instance.CreateHandCard(hostCards, GuestCards);
    }

    public void SendMoveCard(Entity.AttackType atkType, int cardId) {
        Entity.PlayerType playerType =
            IsMasterClient ? Entity.PlayerType.Player : Entity.PlayerType.Enemy;
       _photonView.RPC("PunMoveCard", RpcTarget.Others,
            BattleManager.Instance.UserId, playerType, atkType, cardId);
    }

    [PunRPC]
    private void PunMoveCard(string userId, Entity.PlayerType playerType, Entity.AttackType atkType, int cardId) {
        //Debug.Log(string.Format("MyUserID{0} TargetUserID[{1}]", BattleManager.Instance.UserId, userId));
        if (BattleManager.Instance.UserId == userId) {
            return;
        }

        //Debug.Log(string.Format("Player[{0}] AtkType[{1}] CardID[{2}]", playerType, atkType, cardId));

        MultiPlayerPresenter? mp = (MultiPlayerPresenter)BattleManager.Instance.EnemyPresenter;
        mp?.MoveHandCard(playerType, atkType, cardId);
    }

    public void SendDecidePressed() {
        if (GameManager.Instance.BattleMode == Entity.BattleMode.Solo) {
            PunSecidePressed(BattleManager.Instance.UserId);
            return;
        }
        _photonView.RPC("PunSecidePressed", RpcTarget.Others,
            BattleManager.Instance.UserId);
    }

    [PunRPC]
    private void PunSecidePressed(string userId) {
        if (BattleManager.Instance.UserId == userId) {
            return;
        }

        BattleManager.Instance.EnemyPresenter.DecidePresenter.OnClickDecide();
    }

    public void SendStamp(string fileName) {
        _photonView.RPC("PunStamp", RpcTarget.Others, fileName);
    }

    [PunRPC]
    private void PunStamp(string fileName) {
        MultiPlayerPresenter? mp = BattleManager.Instance.EnemyPresenter as MultiPlayerPresenter;
        mp?.PlayStampAnime(fileName);
    }
}
