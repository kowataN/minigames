﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class Thinker
{
    private PlayerModel _playerModel = default;
    private FieldView _fieldView = default;

    private State.EnemyStateMain _state = new State.EnemyStateMain();

    public enum ThinkStatus {
        kIdle, kThink, kEnd
    };
    private ReactiveProperty<ThinkStatus> _ThinkStatus = new ReactiveProperty<ThinkStatus>(ThinkStatus.kIdle);
    public ReactiveProperty<ThinkStatus> RxThinkStatus => _ThinkStatus;

    public Thinker(PlayerModel playerModel, FieldView fieldView)
    {
        _playerModel = playerModel;
        _fieldView = fieldView;

        Init();
    }

    public void Init()
    {
        _state.Init();
        _ThinkStatus.Value = ThinkStatus.kThink;
    }

    public void Think()
    {
        _state.Execute();
    }
}
