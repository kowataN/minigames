﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class EnemyStateMain
    {
        private StateBase _state = default;

        public void Init()
        {
            _state = new EnemyStateInit(StateInit);
        }

        /// <summary>
        /// ステートマシンを実行します。
        /// </summary>
        public void Execute()
        {
            if (_state != null)
            {
                DebugView.Instance?.SetEnemyMainStateString(_state.GetStateName());
                _state.Execute();
            }
        }

        private void StateInit()
        {
            if (_state.StateEnd)
            {
                _state = new EnemyStateLoop(StateThink);
            }
        }

        private void StateThink()
        {
            if (_state.StateEnd)
            {
                _state = null;
            }
        }
    }
}
