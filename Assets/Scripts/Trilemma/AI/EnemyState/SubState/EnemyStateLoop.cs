﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace State
{
    public class EnemyStateLoop : StateBase
    {
        private SubState _subState = default;

        private EnemyPresenter _presenter;

        public EnemyStateLoop(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public void Init()
        {
            _subState.ChangeState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetEnemySubStateString(_subState.GetStateName());
            _subState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        /// <summary>
        /// 各種初期化
        /// </summary>
        private void ExecuteInit()
        {
            _presenter = (EnemyPresenter)BattleManager.Instance.EnemyPresenter;
            _subState.ChangeState(ExecuteLoop);
        }

        private void ExecuteLoop()
        {
            // 手札が3枚ならランダムで1枚移動させる
            int handCardCount = _presenter.HandPresenter.GetCardCount();
            int handCardIndex = Random.Range(0, handCardCount);
            if (handCardCount >= 2)
            {
                // カード移動
                int dropIndex = 0;
                if (handCardCount == 3)
                {
                    dropIndex = Random.Range(0, 2);
                }
                else
                {
                    dropIndex = _presenter.CardDropPresenter.GetEmptyChildIndex();
                }

                SoundManager.Instance.PlayMoveCard();
                _presenter.MoveHandCard(handCardIndex, dropIndex, EndCardMove);
                _subState.ChangeState(null);
            }
            else
            {
                // 手札が1枚になったら一旦終了
                _subState.ChangeState(ExecuteOnClickDecideButton);
            }
        }

        private void EndCardMove()
        {
            SoundManager.Instance.PlaySetCard();
            _subState.ChangeState(ExecuteLoop);
        }

        private void ExecuteOnClickDecideButton()
        {
            _presenter.DecidePresenter.OnClickDecide();
            _subState.ChangeState(ExecuteEnd);
        }

        private void ExecuteEnd()
        {
            StateEnd = true;
        }
    }
}
