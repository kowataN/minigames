﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class EnemyStateInit : StateBase
    {
        private SubState _subState = default;

        public EnemyStateInit(executeState execute) : base(execute)
        {
            _subState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            DebugView.Instance?.SetEnemySubStateString(_subState.GetStateName());
            _subState.Execute();
            ExecDelegate?.Invoke();
        }

        /// <summary>
        /// 各種初期化
        /// </summary>
        private void ExecuteInit()
        {
            _subState.ChangeState(ExecuteEnd);
        }

        private void ExecuteEnd()
        {
            StateEnd = true;
            _subState.ChangeState(null);
        }
    }
}