﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : SingletonMonoBhv<GameManager> {
    /// <summary>
    /// 入力制御
    /// </summary>
    private LockManager _lockManager = new LockManager();
    public LockManager LockManager => _lockManager;

    [SerializeField] private GameObject _viewParent = default;
    [SerializeField] private TitleView _titleView = default;
    [SerializeField] private MatchingView _matchingView = default;
    [SerializeField] private SettingView _settingView = default;

    private Entity.ViewType _crtViewType = Entity.ViewType.Title;
    private Entity.BattleMode _battleMode = Entity.BattleMode.Solo;

    private System.Random _random = new System.Random();
    public System.Random Random => _random;

    public Entity.BattleMode BattleMode {
        get { return _battleMode; }
        set { _battleMode = value; }
    }

    private Dictionary<Entity.ViewType, float> _viewPosition = new Dictionary<Entity.ViewType, float>();
    private Dictionary<Entity.ViewType, Action> _viewAction = new Dictionary<Entity.ViewType, Action>();

    private void Awake() {
        Application.targetFrameRate = 60;

        // 各画面の表示位置
        _viewPosition.Add(Entity.ViewType.Title, 0.0f);
        _viewPosition.Add(Entity.ViewType.Matching, -1500.0f);
        _viewPosition.Add(Entity.ViewType.Setting, -3000.0f);
        _viewPosition.Add(Entity.ViewType.Battle, -4500.0f);

        // 各画面の初期化
        _viewAction.Add(Entity.ViewType.Title, () => {
            GameManager.Instance.LockManager.Lock();

            Fader.Instance.BlackIn(2.0f, () => {
                Fader.Instance.SetActiveFade(false);
                GameManager.Instance.LockManager.UnLock();
            });
        });

        _viewAction.Add(Entity.ViewType.Matching, () => {
            GameManager.Instance.LockManager.Lock();

            _matchingView.Init();

            Fader.Instance.SetActiveFade(false);
            GameManager.Instance.LockManager.UnLock();
        });

        _viewAction.Add(Entity.ViewType.Setting, () => {
            GameManager.Instance.LockManager.Lock();

            _settingView.Init();

            Fader.Instance.SetActiveFade(false);
            GameManager.Instance.LockManager.UnLock();
        });

        _viewAction.Add(Entity.ViewType.Battle, () => {
            BattleManager.Instance.Create();
            BattleManager.Instance.BattleStart();
        });

        SetViewPosition(Entity.ViewType.Title);
    }

    public void SetViewPosition(Entity.ViewType viewType) {
        _crtViewType = viewType;

        // 画面の位置調整
        float posX = _viewPosition[_crtViewType];
        _viewParent.transform.localPosition = new Vector2(posX, 0.0f);

        // 画面の初期化実行
        _viewAction[_crtViewType]?.Invoke();
    }
}
