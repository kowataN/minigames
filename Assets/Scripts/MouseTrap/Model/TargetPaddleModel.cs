﻿using System.Collections;
using UniRx;
using UnityEngine;

namespace Model
{
    public class TargetPaddleModel : MonoBehaviour
    {
        public ReactiveProperty<Color> Target = new ReactiveProperty<Color>();
    }
}
