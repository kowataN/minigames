﻿using DG.Tweening;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    void Start() { }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = new Ray();
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                float crtY = transform.eulerAngles.y;
                if (gameObject.name == hit.collider.gameObject.name)
                {
                    OnRotate(new Vector2(Input.GetAxis("Mouse X"), crtY+Input.GetAxis("Mouse Y")));
                    //transform.DORotate(new Vector3(0, crtY + 90, 90.0f), 1.0f);
                }
            }
        }
    }

    private void OnRotate(Vector2 delta)
    {
        Debug.Log("OnRotate : " + delta.ToString());
        float deltaAngle = delta.magnitude;
        if (Mathf.Approximately(deltaAngle, 0.0f))
        {
            return;
        }

        Transform cameraTrans = Camera.main.transform;
        Vector3 deltaWorld = cameraTrans.right * delta.x + cameraTrans.up * delta.y;

        Vector3 axisWorld = Vector3.Cross(deltaWorld, cameraTrans.forward).normalized;

        transform.Rotate(axisWorld, deltaAngle, Space.World);
    }
}
