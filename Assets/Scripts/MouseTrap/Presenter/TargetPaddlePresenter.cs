﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

using Model;
using View;

namespace Presenter
{
    public class TargetPaddlePresenter : MonoBehaviour
    {
        private TargetPaddleModel _model;
        private TargetPaddleView _view;

        public void Init(TargetPaddleModel model, TargetPaddleView view)
        {
            if (model == null)
            {
                Debug.LogError("[TargetPaddlePresenter::Init] model is null");
                return;
            }
            if (view == null)
            {
                Debug.LogError("[TargetPaddlePresenter::Init] view is null");
                return;
            }

            _model = model;
            _view = view;

            // Modelの色が変更されたらViewの色を更新する
            _model.Target.ObserveEveryValueChanged(x => x.Value)
                .Subscribe();
        }
    }
}
