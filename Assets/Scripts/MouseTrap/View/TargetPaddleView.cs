﻿using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class TargetPaddleView : MonoBehaviour
    {
        [SerializeField] private Text _targetPaddle;
        public Text TargetPaddle => _targetPaddle;

        private void Awake()
        {
            if (_targetPaddle == null)
            {
                Debug.LogError("[TargetPaddleView::Awake] _targetPaddle is null");
            }
        }
    }
}
